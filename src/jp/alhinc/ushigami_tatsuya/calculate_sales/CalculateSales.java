package jp.alhinc.ushigami_tatsuya.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class CalculateSales {//売り上げ集計システムにおける要素



	public static void main(String[] args) {//メインメソッドの定義
		if(args.length == 0) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		HashMap<String, String> idName = new HashMap<String, String>();
		HashMap<String, Long> idSale = new HashMap<String, Long>();
        List<File> catchData = new ArrayList<File>();//各変数の定義

        if(!InputIdmethod(args[0], idName, idSale)){
			System.out.println("予期せぬエラーが発生しました");
			return;
        }

        File sales = new File(args[0]);//コマンドライン引数よりFileを定義
    	File[] saleData = sales.listFiles();//引数より全データを配列に格納
    	for(int i=0; i<saleData.length; i++) {//要素数分繰り返し処理
    		if(saleData[i].isFile()&&saleData[i].getName().matches("^[0-9]{8}.rcd$")) {//ファイルかつ[8桁数字].
    			catchData.add(saleData[i]);
    		}
    	}
    	for(int i=0; i<catchData.size()-1;i++) {
    		String code = catchData.get(i).getName().substring(0,8);
    		String code2 = catchData.get(i+1).getName().substring(0,8);
    		if(Integer.parseInt(code2) - Integer.parseInt(code) != 1) {
    			System.out.println("売上ファイル名が連番になっていません");
    			return;
    		}
    	}
    	for(int p =0;p<catchData.size(); p++){
    		List <String>strData = new ArrayList<String>();
    		BufferedReader br2 = null;
    		try {
    			FileReader fr2 = new FileReader(catchData.get(p));
    			br2 = new BufferedReader(fr2);
    			String line2;
    			while((line2 = br2.readLine()) != null) {
    				strData.add(line2);
    			}
    			if(strData.size() != 2) {
    				System.out.println(catchData.get(p).getName()+"のフォーマットが不正です");
    				return;
    			}
    			if(!idSale.containsKey(strData.get(0))) {
    				System.out.println(catchData.get(p).getName()+"の支店コードが不正です");
    				return;
    			}
    			String key1 = strData.get(0);
   				if(!strData.get(1).matches("^[0-9]{0,10}$")) {
   					System.out.println("予期せぬエラーが発生しました");
   					return;
   				}    				long key2 = Long.parseLong(strData.get(1));
    			long sum = (idSale.get(key1) + key2);
    			if(sum > 10000000000L) {
    				System.out.println("合計合計が10桁を超えました");
   					return ;
   				}
   				idSale.put(key1, sum);
   			}
    		catch (IOException e) {
    			System.out.println("予期せぬエラーが発生しました");
    			return ;
    		}
    		finally {
    			if(br2 != null) {
    				try {
    					br2.close();
   					}
   					catch(IOException e) {
   						System.out.println("予期せぬエラーが発生しました");
   						return;
   					}
    			}
    		}
    	}


		if(!Outputmethod(args[0], idName, idSale)){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
	}

	//支店定義ファイル読み込みのメソッド
	public static boolean InputIdmethod(String file, HashMap<String, String> idName,  HashMap<String, Long> idSale) {
		BufferedReader br = null;//BW変数の定義
		try {
			File fileFile = new File(file, "branch.lst");//支店定義ファイル読み込み
			if(!fileFile.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}
			FileReader fr = new FileReader(fileFile);
			br = new BufferedReader(fr);//読み込んだ情報を変数に保存
			String line;
			while((line = br.readLine()) != null){//ファイル情報がある場合一行ずつ読み込み
				String[] data = line.split(",");//,より前か後がでデータを分割
				if(data.length != 2||!data[0].matches("^[0-9]{3}$")) {//dataの要素が2つではない、または支店コードが3桁の数字ではないなら
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				idName.put(data[0], data[1]);
				idSale.put(data[0], 0L);//各情報をマップに追加
			}
		}
		catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}
		finally {
			if(br != null) {
				try {
					br.close();
				}
				catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	//支店別集計ファイル出力のメソッド
	public static boolean Outputmethod(String file, HashMap<String, String> idName,  HashMap<String, Long> idSale) {
		BufferedWriter bw = null;
		try {
			File fileFile = new File(file,"branch.out") ;
			FileWriter fr = new FileWriter(fileFile);
			bw = new BufferedWriter(fr);
			for(String keyA:idName.keySet()) {
				bw.write( (keyA)+ "," +  idName.get(keyA) + "," + idSale.get(keyA));
				bw.newLine();
			}
		}
		catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}
		finally {
			if(bw != null) {
				try {
					bw.close();
				}
				catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

}




